#![doc = include_str!("../README.md")]

pub mod bytes;
pub mod io;
pub mod numbers;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Endianness {
    Little,
    Big,
}
