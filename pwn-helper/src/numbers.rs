use std::fmt::{Display, LowerHex};

use crate::Endianness;

pub fn hex<Value: LowerHex>(value: Value) -> String {
    format!("{:x}", value)
}

pub fn decimal<Value: Display>(value: Value) -> String {
    format!("{}", value)
}

pub fn undecimal8(value: &[u8]) -> u8 {
    String::from_utf8_lossy(value)
        .as_ref()
        .parse::<u8>()
        .unwrap()
}

pub fn undecimal16(value: &[u8]) -> u16 {
    String::from_utf8_lossy(value)
        .as_ref()
        .parse::<u16>()
        .unwrap()
}

pub fn undecimal32(value: &[u8]) -> u32 {
    String::from_utf8_lossy(value)
        .as_ref()
        .parse::<u32>()
        .unwrap()
}

pub fn undecimal64(value: &[u8]) -> u64 {
    String::from_utf8_lossy(value)
        .as_ref()
        .parse::<u64>()
        .unwrap()
}

pub fn undecimal128(value: &[u8]) -> u128 {
    String::from_utf8_lossy(value)
        .as_ref()
        .parse::<u128>()
        .unwrap()
}

pub fn p8(value: u8) -> Vec<u8> {
    vec![value]
}

pub fn unhex8(value: &[u8]) -> u8 {
    u8::from_str_radix(String::from_utf8_lossy(&value[2..]).as_ref(), 16).unwrap()
}

pub fn u8(value: &[u8]) -> u8 {
    value[0]
}

pub fn p16(value: u16, endianness: Endianness) -> [u8; 2] {
    match endianness {
        Endianness::Little => value.to_le_bytes(),
        Endianness::Big => value.to_be_bytes(),
    }
}

pub fn unhex16(value: &[u8]) -> u16 {
    u16::from_str_radix(String::from_utf8_lossy(&value[2..]).as_ref(), 16).unwrap()
}

pub fn u16(value: &[u8], endianness: Endianness) -> u16 {
    match endianness {
        Endianness::Little => {
            u16::from_le_bytes(value.try_into().expect("byte array not big enough"))
        }
        Endianness::Big => u16::from_be_bytes(value.try_into().expect("byte array not big enough")),
    }
}

pub fn p32(value: u32, endianness: Endianness) -> [u8; 4] {
    match endianness {
        Endianness::Little => value.to_le_bytes(),
        Endianness::Big => value.to_be_bytes(),
    }
}

pub fn unhex32(value: &[u8]) -> u32 {
    u32::from_str_radix(String::from_utf8_lossy(&value[2..]).as_ref(), 16).unwrap()
}

pub fn u32(value: &[u8], endianness: Endianness) -> u32 {
    match endianness {
        Endianness::Little => {
            u32::from_le_bytes(value.try_into().expect("byte array not big enough"))
        }
        Endianness::Big => u32::from_be_bytes(value.try_into().expect("byte array not big enough")),
    }
}

pub fn p64(value: u64, endianness: Endianness) -> [u8; 8] {
    match endianness {
        Endianness::Little => value.to_le_bytes(),
        Endianness::Big => value.to_be_bytes(),
    }
}

pub fn unhex64(value: &[u8]) -> u64 {
    u64::from_str_radix(String::from_utf8_lossy(&value[2..]).as_ref(), 16).unwrap()
}

pub fn u64(value: &[u8], endianness: Endianness) -> u64 {
    match endianness {
        Endianness::Little => {
            u64::from_le_bytes(value.try_into().expect("byte array not big enough"))
        }
        Endianness::Big => u64::from_be_bytes(value.try_into().expect("byte array not big enough")),
    }
}

pub fn p128(value: u128, endianness: Endianness) -> [u8; 16] {
    match endianness {
        Endianness::Little => value.to_le_bytes(),
        Endianness::Big => value.to_be_bytes(),
    }
}

pub fn unhex128(value: &[u8]) -> u128 {
    u128::from_str_radix(String::from_utf8_lossy(&value[2..]).as_ref(), 16).unwrap()
}

pub fn u128(value: &[u8], endianness: Endianness) -> u128 {
    match endianness {
        Endianness::Little => {
            u128::from_le_bytes(value.try_into().expect("byte array not big enough"))
        }
        Endianness::Big => {
            u128::from_be_bytes(value.try_into().expect("byte array not big enough"))
        }
    }
}
