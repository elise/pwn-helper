#![doc = include_str!("../README.md")]

use core::panic;

use proc_macro::TokenStream;
use proc_macro2::Span;
use syn::{parse::Parse, parse_macro_input, Lit};

enum Operand {
    Byte(syn::LitByte),
    ByteStr(syn::LitByteStr),
    Int(syn::LitInt),
}

impl Parse for Operand {
    fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
        match input.parse::<syn::Lit>() {
            Ok(Lit::Byte(x)) => Ok(Operand::Byte(x)),
            Ok(Lit::ByteStr(x)) => Ok(Operand::ByteStr(x)),
            Ok(Lit::Int(x)) => Ok(Operand::Int(x)),
            _ => Err(syn::Error::new(
                input.span(),
                "Missing operand (Either byte literal, byte str, or int)",
            )),
        }
    }
}

enum Operation {
    Multiply,
    Addition,
}

struct ByteOperation {
    pub lhs: Operand,
    pub operation: Operation,
    pub rhs: Operand,
    pub span: Span,
}

impl Parse for ByteOperation {
    fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
        let span = input.span();
        let lhs = Operand::parse(input)?;

        let operation = match input.cursor().punct() {
            Some((x, _)) => match x.as_char() {
                '*' => {
                    input.parse::<syn::UnOp>()?;
                    Operation::Multiply
                }
                '+' => {
                    input.parse::<syn::BinOp>()?;
                    Operation::Addition
                }
                _ => {
                    return Err(syn::Error::new(
                        input.span(),
                        "Missing operand (Expected either `+` or `*`)",
                    ))
                }
            },
            None => {
                return Err(syn::Error::new(
                    input.span(),
                    "Missing operand (Expected either `+` or `*`)",
                ))
            }
        };

        let rhs = Operand::parse(input)?;

        Ok(Self {
            lhs,
            operation,
            rhs,
            span,
        })
    }
}

#[proc_macro]
pub fn bytes(ts: TokenStream) -> TokenStream {
    let input = parse_macro_input!(ts as ByteOperation);

    let lhs = match input.lhs {
        Operand::Byte(b) => vec![b.value()],
        Operand::ByteStr(b) => b.value(),
        Operand::Int(_) => panic!("Int on left side"),
    };

    let output = match input.operation {
        Operation::Multiply => match input.rhs {
            Operand::Byte(_) | Operand::ByteStr(_) => panic!("Failed to multiply bytes with bytes"),
            Operand::Int(i) => {
                let parsed_rhs = i
                    .base10_parse::<usize>()
                    .expect("Failed to parse right hand side");
                let mut out: Vec<u8> = Vec::with_capacity(lhs.len() * parsed_rhs);
                for _ in 0..parsed_rhs {
                    for x in &lhs {
                        out.push(*x);
                    }
                }
                out
            }
        },
        Operation::Addition => match input.rhs {
            Operand::Byte(b) => {
                let mut x = lhs;
                x.push(b.value());
                x
            }
            Operand::ByteStr(b) => {
                let mut x = lhs;
                x.append(&mut b.value());
                x
            }
            Operand::Int(_) => panic!("Failed to add bytes with int"),
        },
    };

    let lit_output = syn::LitByteStr::new(&output, input.span);

    let out = quote::quote! {
        #lit_output
    };

    out.into()
}

struct PackOperation {
    pub lhs: syn::LitInt,
    pub bits: syn::LitInt,
    pub span: Span,
}

impl Parse for PackOperation {
    fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
        let span = input.span();
        let lhs = input.parse()?;
        input.parse::<syn::Token!(,)>()?;
        let bits = input.parse()?;

        Ok(Self { lhs, bits, span })
    }
}

#[proc_macro]
pub fn pack(ts: TokenStream) -> TokenStream {
    let input = parse_macro_input!(ts as PackOperation);

    let bits = input
        .bits
        .base10_parse::<usize>()
        .expect("Failed to parse left hand side as int");

    if bits == 0 || bits % 8 != 0 {
        panic!("Right hand side is not a multiple of 8")
    }

    if bits > 128 {
        panic!("Maximum of 128 bits supported")
    }

    let lhs_bytes = {
        match bits {
            8 => input
                .lhs
                .base10_parse::<u8>()
                .expect("Failed to parse left hand side as int")
                .to_ne_bytes()
                .to_vec(),
            16 => input
                .lhs
                .base10_parse::<u16>()
                .expect("Failed to parse left hand side as int")
                .to_ne_bytes()
                .to_vec(),
            32 => input
                .lhs
                .base10_parse::<u32>()
                .expect("Failed to parse left hand side as int")
                .to_ne_bytes()
                .to_vec(),
            64 => input
                .lhs
                .base10_parse::<u64>()
                .expect("Failed to parse left hand side as int")
                .to_ne_bytes()
                .to_vec(),
            128 => input
                .lhs
                .base10_parse::<u128>()
                .expect("Failed to parse left hand side as int")
                .to_ne_bytes()
                .to_vec(),
            _ => unreachable!(),
        }
    };

    let bytes = bits / 8;

    if lhs_bytes.len() > bytes {
        panic!("Literal {} out of range {}", lhs_bytes.len(), bytes);
    }

    let mut output = Vec::with_capacity(bytes);

    if cfg!(target_endian = "big") {
        let cur_len = lhs_bytes.len();

        if cur_len < bytes {
            output.resize(bytes - cur_len, 0);
        }

        for x in lhs_bytes {
            output.push(x);
        }
    } else {
        let cur_len = lhs_bytes.len();

        for x in lhs_bytes {
            output.push(x);
        }

        if cur_len < bytes {
            output.resize(bytes, 0);
        }
    }

    let lit_output = syn::LitByteStr::new(&output, input.span);

    let out = quote::quote! {
        #lit_output
    };

    out.into()
}
